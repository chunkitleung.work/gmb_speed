

from re import L
import pandas as pd
import datetime as dt
import datetime
from datetime import timedelta,timezone
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from cassandra.query import dict_factory
import json
import time

auth_provider = PlainTextAuthProvider(username='cassandra', password='cassandra')
cluster = Cluster(contact_points=['158.132.175.131'], port='9042',
    auth_provider=auth_provider)

session = cluster.connect('hki_gmb')
session.row_factory = dict_factory

def select_last_10_min_data(route_id,gen_date,route_seq,stop_seq, now_time = (datetime.datetime.now()- timedelta(minutes=10)).isoformat()):

    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {} and fetch_moment >= '{}+0800' and gen_time <= fetch_monent  limit 40;".format(route_id,gen_date,route_seq,stop_seq, now_time )
    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {} and fetch_moment >= '{}+0800' ;".format(route_id,gen_date,route_seq,stop_seq, now_time )
    sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = 2003472 and gen_date = '2022-08-18' and route_seq = 1 and stop_seq = 3 and fetch_moment >= '2022-08-18 15:55:00+0800' and fetch_moment <= '2022-08-18 17:10:00+0800' ;"
    
    # .format(route_id,gen_date,route_seq,stop_seq, now_time )
    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '2022-08-10' and route_seq = {} and stop_seq = {}  ;".format(route_id,route_seq,stop_seq, now_time )


    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {} ;".format(route_id,gen_date,route_seq,stop_seq)




    dictionary ={ "fetch_moment":[], "eta_1_diff":[], "eta_1_time":[],"eta_1_remark":[], "eta_2_diff":[], "eta_2_time":[],"eta_2_remark":[], "eta_3_diff":[], "eta_3_time":[],"eta_3_remark":[],"gen_time":[]}
    # "route_id" :[], "route_seq" :[], "stop_seq":[],



    for row in session.execute(sql_query):

        dictionary["fetch_moment"].append(row['fetch_moment'])
        dictionary["eta_1_diff"].append(row['eta_1_diff'])
        dictionary["eta_1_time"].append(row['eta_1_time'])
        dictionary["eta_1_remark"].append(row['eta_1_remark'])
        dictionary["eta_2_diff"].append(row['eta_2_diff'])
        dictionary["eta_2_time"].append(row['eta_2_time'])
        dictionary["eta_2_remark"].append(row['eta_2_remark'])
        dictionary["eta_3_diff"].append(row['eta_3_diff'])
        dictionary["eta_3_time"].append(row['eta_3_time'])
        dictionary["eta_3_remark"].append(row['eta_3_remark'])
        dictionary["gen_time"].append(row['gen_time'])

    df = pd.DataFrame(dictionary)


    df['fetch_moment'] = df['fetch_moment'].apply(convert_timezone)
    df['eta_1_time'] = df['eta_1_time'].apply(convert_timezone)
    df['eta_2_time'] = df['eta_2_time'].apply(convert_timezone)
    df['eta_3_time'] = df['eta_3_time'].apply(convert_timezone)
    df['gen_time'] = df['gen_time'].apply(convert_timezone)

    return df



def convert_timezone(timestamp):
   return timestamp.tz_localize(tz = 'UTC').tz_convert(tz ='Asia/Hong_Kong')


if __name__ == "__main__":
    
    dfa = select_last_10_min_data(str(2003472),'2022-08-18','1','3',(datetime.datetime.now()- timedelta(hours=4)).isoformat())
    print(dfa)

    dfa.to_csv('18_08_2022_stop3_data.csv',index=False)
