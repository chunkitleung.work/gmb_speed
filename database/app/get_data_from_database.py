# from cassandra.cluster import Cluster
# import pandas as pd
# from sqlalchemy import create_engine

# engine = create_engine("cassandra:///?Database=hki_gmb&Port=9042&Server=158.132.175.131")

# df = pd.read_sql("SELECT * from eta_data where route_id = 2006408 and gen_date = '2022-08-10' and route_seq = 1 and stop_seq = 5;")
 
# print(df)


from re import L
import pandas as pd
import datetime as dt
import datetime
from datetime import timedelta,timezone
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from cassandra.query import dict_factory
import json
import time


auth_provider = PlainTextAuthProvider(username='cassandra', password='cassandra')
cluster = Cluster(contact_points=['158.132.175.131'], port='9042',
    auth_provider=auth_provider)

session = cluster.connect('hki_gmb')
session.row_factory = dict_factory


def get_route_combination():
    
    with open("../data/minibus_info/{}/GMB_info_{}.json".format(datetime.date.today(),datetime.date.today())) as f:
        data = json.load(f)

    route_combination = []

    for route_no in data[0]["routes"]:
        for route in route_no["route_info"]:
            for direction in route['directions']:
                for stop in direction["stop_info"]:
                    route_combination.append({
                        "route_id" : route["route_id"],
                        "route_seq" : direction["route_seq"],
                        "stop_seq" : stop["stop_seq"]

                    })


    return route_combination


start_time_of_today = datetime.datetime.now()

def select_last_10_min_data(route_id,gen_date,route_seq,stop_seq, now_time = (datetime.datetime.now()- timedelta(minutes=10)).isoformat()):

    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {} and fetch_moment >= '{}+0800' and gen_time <= fetch_monent  limit 40;".format(route_id,gen_date,route_seq,stop_seq, now_time )
    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {} and fetch_moment >= '{}+0800' ;".format(route_id,gen_date,route_seq,stop_seq, now_time )
    sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {}  ;".format(route_id,gen_date,route_seq,stop_seq, now_time )
    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '2022-08-10' and route_seq = {} and stop_seq = {}  ;".format(route_id,route_seq,stop_seq, now_time )


    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {} ;".format(route_id,gen_date,route_seq,stop_seq)




    dictionary ={ "fetch_moment":[], "eta_1_diff":[], "eta_1_time":[],"eta_1_remark":[], "eta_2_diff":[], "eta_2_time":[],"eta_2_remark":[], "eta_3_diff":[], "eta_3_time":[],"eta_3_remark":[],"gen_time":[]}
    # "route_id" :[], "route_seq" :[], "stop_seq":[],



    for row in session.execute(sql_query):

        dictionary["fetch_moment"].append(row['fetch_moment'])
        dictionary["eta_1_diff"].append(row['eta_1_diff'])
        dictionary["eta_1_time"].append(row['eta_1_time'])
        dictionary["eta_1_remark"].append(row['eta_1_remark'])
        dictionary["eta_2_diff"].append(row['eta_2_diff'])
        dictionary["eta_2_time"].append(row['eta_2_time'])
        dictionary["eta_2_remark"].append(row['eta_2_remark'])
        dictionary["eta_3_diff"].append(row['eta_3_diff'])
        dictionary["eta_3_time"].append(row['eta_3_time'])
        dictionary["eta_3_remark"].append(row['eta_3_remark'])
        dictionary["gen_time"].append(row['gen_time'])

    df = pd.DataFrame(dictionary)


    df['fetch_moment'] = df['fetch_moment'].apply(convert_timezone)
    df['eta_1_time'] = df['eta_1_time'].apply(convert_timezone)
    df['eta_2_time'] = df['eta_2_time'].apply(convert_timezone)
    df['eta_3_time'] = df['eta_3_time'].apply(convert_timezone)
    df['gen_time'] = df['gen_time'].apply(convert_timezone)

    return df



def convert_timezone(timestamp):
   return timestamp.tz_localize(tz = 'UTC').tz_convert(tz ='Asia/Hong_Kong')


if __name__ == "__main__":
    
    dfa = select_last_10_min_data(str(2003472),'2022-08-18','1','3',(datetime.datetime.now()- timedelta(hours=4)).isoformat())
    dfa.to_csv('stop3_data.csv',index=False)




    # # print(select_last_10_min_data(str(2006408),'2022-08-18','1','1'))

    # counter = 0
    # miss_counter = 0
    # print(len(get_route_combination()))

    
    # for route in get_route_combination():
    #     try:

    #         df = select_last_10_min_data(str(route['route_id']),str(datetime.date.today()),route['route_seq'],route['stop_seq'])

    #         # df['fetch_moment'] = df['fetch_moment'].apply(convert_timezone)
    #         # print(df['fetch_moment'])
    #         # df['eta_1_time'] = df['eta_1_time'].apply(convert_timezone)
    #         # df['eta_2_time'] = df['eta_2_time'].apply(convert_timezone)
    #         # df['eta_3_time'] = df['eta_3_time'].apply(convert_timezone)
    #         # df['gen_time'] = df['gen_time'].apply(convert_timezone)


            
    #         for index_,row in df.iterrows():
    #             # print(type(row))
    #             # print(row)

    #             # print(row['fetch_moment'])
    #             # print(row['gen_time'])



    #             if row['fetch_moment']  < row['gen_time'] - timedelta(seconds = 10) :
    #                 counter +=1

    #                 print("??????????????????????????????")
    #                 print("??????????????????????????????")
    #                 print(row)
    #                 print("??????????????????????????????")
    #                 print("??????????????????????????????")
    #                 print("??????????????????????????????")

    #                 df.drop(index_,inplace = True)

    #             # print(df)
        
    #     except:
    #         miss_counter += 1
            
    

    # print("miss count :" + str(miss_counter))


    # # print(df.iloc[0,0].tz_localize(tz = 'UTC').tz_convert(tz ='Asia/Hong_Kong'))
    # # print(df.iloc[0,0].astimezone(tz ='Asia/Hong_Kong'))
    # # print(df.iloc[0,0].tz_localize(tz='Asia/Hong_Kong'))
    # # print(df.iloc[0,0].tz_convert(tz='Asia/Hong_Kong'))



        
    


                