import pandas as pd
import json
import glob 
from datetime import datetime,timedelta,date
import dateutil.parser
import numpy as np
from re import L

import datetime as dt
import datetime
from datetime import timedelta,timezone
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from cassandra.query import dict_factory




def get_arrival_time(dataframe):
    arrival_time_list = []
    
    for i in range(len(dataframe)-1):
        if not(dataframe.iloc[i,1] == 0)  and (dataframe.iloc[i+1,1] == 0 )  :
           # list.append(dataframe1.iloc[j+i,5])
            arrival_time_list.append(dataframe.iloc[i+1,2])
        
    return arrival_time_list



auth_provider = PlainTextAuthProvider(username='cassandra', password='cassandra')
cluster = Cluster(contact_points=['158.132.175.131'], port='9042',
    auth_provider=auth_provider)

session = cluster.connect('hki_gmb')
session.row_factory = dict_factory

def convert_timezone(timestamp):
    return timestamp.tz_localize(tz = 'UTC').tz_convert(tz ='Asia/Hong_Kong')


def select_data(gen_date,route_id,route_seq,stop):

    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {} and fetch_moment >= '{}+0800' and gen_time <= fetch_monent  limit 40;".format(route_id,gen_date,route_seq,stop_seq, now_time )
    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {} and fetch_moment >= '{}+0800' ;".format(route_id,gen_date,route_seq,stop_seq, now_time )
    
    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = 2003472 and gen_date = '{}' and route_seq = 1 and stop_seq = {} and fetch_moment >= '{} 16:00:00+0800' and fetch_moment <= '{} 17:00+0800' ;".format(gen_date,stop,gen_date,gen_date)
    
    
    sql_query = "SELECT * FROM hki_gmb.route30_eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {} and fetch_moment >= '{} 04:30:00+0800' and fetch_moment <= '{} 00:30:00+0800' ;".format(route_id, gen_date,route_seq,stop,gen_date, str(datetime.datetime.strptime(gen_date, "%Y-%m-%d").date() + datetime.timedelta(days = 1)))
#    sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {}  ;".format(route_id, gen_date,route_seq,stop)



#     sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = 2003472 and gen_date = '{}' and route_seq = 1 and stop_seq = {} ;".format(gen_date,stop)


    # .format(route_id,gen_date,route_seq,stop_seq, now_time )
    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '2022-08-10' and route_seq = {} and stop_seq = {}  ;".format(route_id,route_seq,stop_seq, now_time )


    # sql_query = "SELECT * FROM hki_gmb.eta_data where route_id = {} and gen_date = '{}' and route_seq = {} and stop_seq = {} ;".format(route_id,gen_date,route_seq,stop_seq)

    dictionary ={ "fetch_moment":[], "eta_1_diff":[], "eta_1_time":[],"eta_1_remark":[], "eta_2_diff":[], "eta_2_time":[],"eta_2_remark":[], "eta_3_diff":[], "eta_3_time":[],"eta_3_remark":[],"gen_time":[]}
    # "route_id" :[], "route_seq" :[], "stop_seq":[],



    for row in session.execute(sql_query):

        dictionary["fetch_moment"].append(row['fetch_moment'])
        dictionary["eta_1_diff"].append(row['eta_1_diff'])
        dictionary["eta_1_time"].append(row['eta_1_time'])
        dictionary["eta_1_remark"].append(row['eta_1_remark'])
        dictionary["eta_2_diff"].append(row['eta_2_diff'])
        dictionary["eta_2_time"].append(row['eta_2_time'])
        dictionary["eta_2_remark"].append(row['eta_2_remark'])
        dictionary["eta_3_diff"].append(row['eta_3_diff'])
        dictionary["eta_3_time"].append(row['eta_3_time'])
        dictionary["eta_3_remark"].append(row['eta_3_remark'])
        dictionary["gen_time"].append(row['gen_time'])

    df = pd.DataFrame(dictionary)


    df['fetch_moment'] = df['fetch_moment'].apply(convert_timezone)
    df['eta_1_time'] = df['eta_1_time'].apply(convert_timezone)
    df['eta_2_time'] = df['eta_2_time'].apply(convert_timezone)
    df['eta_3_time'] = df['eta_3_time'].apply(convert_timezone)
    df['gen_time'] = df['gen_time'].apply(convert_timezone)

    return df


# convert map infomation

GMB_route_map = pd.read_excel('MB_30_HV.xls',header=0)
GMB_route_map_length = (GMB_route_map.groupby(['stop_range']).sum()['Shape_Length']*100000)
GMB_route_map_length

def get_stops_list(route_id,route_seq,gen_date):
    auth_provider = PlainTextAuthProvider(username='cassandra', password='cassandra')
    cluster = Cluster(contact_points=['158.132.175.131'], port='9042',
        auth_provider=auth_provider)

    session = cluster.connect('hki_gmb')
    session.row_factory = dict_factory
    
    
    
    sql_query = "SELECT stops from hki_gmb.routeid_routeno_relationship WHERE route_id = {} and route_seq = {} and gen_date = '{}' ;".format(route_id,route_seq, gen_date)

    return session.execute(sql_query).one()['stops']


def create_shifts_allday_df(route_id,route_seq,gen_date):

    # get possible arrivel time
    stops = get_stops_list(route_id,route_seq,gen_date)

    class timelist(object): pass
    route = timelist()

    timelist_dict ={}
    
    for stop in stops[1:]:
        timelist_dict["stop{}".format(stop)] = get_arrival_time(select_data(gen_date,route_id,route_seq,stop)) 
        
        timelist_dict["stop{}".format(stop)].reverse()


    arrival_time_df = pd.DataFrame(columns = stops)

    max_speed = 19.4  ## m/s

    
    # assign stops to the shifts table
    
    
    for stop in stops[:-1]:
        if stop == 1:
            
            pass

        elif stop == 2:
            
            for i in range(len(timelist_dict["stop{}".format(stop)])):
                arrival_time_df.loc[i] = np.NaN
                arrival_time_df.iloc[i,stop-1] = timelist_dict["stop{}".format(stop)][i]

                for j in range(len(timelist_dict["stop{}".format(stop + 1)])):

                    if arrival_time_df.iloc[i,stop-1] + pd.Timedelta(seconds = GMB_route_map_length[stop]/max_speed) < timelist_dict["stop{}".format(stop + 1)][j]:

                        arrival_time_df.iloc[i,stop] = timelist_dict["stop{}".format(stop + 1)][j]
                        break

                    arrival_time_df.iloc[i,stop] = None
                
        else:
            
            for i in range(len(arrival_time_df.iloc[:,stop-1])):
                
                for j in range(len(timelist_dict["stop{}".format(stop + 1)])):
                    
                    if not(arrival_time_df.iloc[i,stop-1] == None):
        
                        if arrival_time_df.iloc[i,stop-1] + pd.Timedelta(seconds = GMB_route_map_length[stop]/max_speed) < timelist_dict["stop{}".format(stop + 1)][j]:

                            arrival_time_df.iloc[i,stop] = timelist_dict["stop{}".format(stop + 1)][j]

                            break

                        arrival_time_df.iloc[i,stop] = None
                
                    else:

                        arrival_time_df.iloc[i,stop] = None
                        
        
        
#    arrange the over-estimate, under-estimate, two-continue shifts
    

    
    for stop in stops[3:]:

        for row_index in arrival_time_df[arrival_time_df.duplicated(subset=[stop])].index:
            
            if row_index == len(arrival_time_df)-1 :
                pass
            
            else:

                for x in timelist_dict["stop{}".format(stop)]:
                    try:

                        if arrival_time_df.iloc[row_index,stop-1] < x < arrival_time_df.iloc[row_index + 1,stop-1]: # and x < arrival_time_df.iloc[row_index,stop -1] :
                            arrival_time_df.iloc[row_index,stop-1] = x
                    except:
                        pass
                                
        for row_index in arrival_time_df[arrival_time_df.duplicated(subset=[stop],keep ='last')].index:
            arrival_time_df =  arrival_time_df.drop(index=(row_index))
        
        arrival_time_df.reset_index(drop=True, inplace=True)
    
    return arrival_time_df     
            

def inverse(obj):
    if obj == None:
        return None
    else:
        return 1/obj


def time_delta_to_sec(time_delta_obj):
    return time_delta_obj.total_seconds()


def update_30shift_table(route_id,route_seq):
    
    date = (datetime.datetime.now() - datetime.timedelta(days =1)).strftime('%Y-%m-%d')

    route_30_shift_df = create_shifts_allday_df(route_id,route_seq,date)
    route_30_shift_df['shift_id'] = range(len(route_30_shift_df))

    
    for stop in get_stops_list(route_id,route_seq,date)[1:-1]:
        route_30_shift_df['stop{}_speed'.format(stop)] = (route_30_shift_df[int(stop)+1] - route_30_shift_df[int(stop)]).apply(time_delta_to_sec).div(other = GMB_route_map_length[int(stop)]).apply(inverse)
    
    route_30_shift_df['total_speed'] = (route_30_shift_df[get_stops_list(route_id,route_seq,date)[-1]] - route_30_shift_df[get_stops_list(route_id,route_seq,date)[1]]).apply(time_delta_to_sec).div(other = GMB_route_map_length[1:].sum()).apply(inverse)

    route_30_shift_df['route_id'] = route_id
    route_30_shift_df['route_seq'] = route_seq
    route_30_shift_df['gen_date'] = date
    

    route_30_shift_df = route_30_shift_df[['route_id','route_seq','gen_date','shift_id',1,2,3,4,5,6,'stop2_speed','stop3_speed','stop4_speed','stop5_speed','total_speed']]
    
    

    route_30_shift_df.to_csv('../data/route_30_speed/route_30_speed_{}.csv'.format(date),index = False)

    route_30_shift_df


if __name__ == "__main__":
    update_30shift_table(2003472,1)
    print("updated route30_speed info")


























